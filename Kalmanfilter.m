close all; clear all; clc;
KalmanMeasurement = 0;
KalmanEstimate = 0.00005;
Current = [161, 149, 138, 142, 153, 138, 151, 162, 147, 154, 140, 133, 160, 163, 129, 153,146,147, 150, 183, 154, 179, 167, 161, 175,156, 149, 156, 172, 143, 155, 139, 164, 133, 143, 149, 133, 142, 157, 149,160, 153, 164, 135, 139, 136, 138, 147, 150, 153, 156, 163, 134, 152, 135, 154, 132, 152, 154, 134, 150, 158, 139, 131, 163, 137, 142, 153, 156, 161, 141, 134, 159, 137, 151, 132, 163, 132, 133, 156, 161, 163, 161, 145, 132, 151, 132, 161, 154, 260, 257, 249, 244, 258, 239, 258, 239, 230, 252,];
dt = 0.005;
Time = [0.50, 1.00, 1.50, 2.00, 2.50, 3.00, 3.50, 4.00, 4.50, 5.00, 5.50, 6.00, 6.50, 7.00, 7.50, 8.00, 8.50, 9.00, 9.50, 10.00, 10.50, 11.00, 11.50, 12.00, 12.50, 13.00, 13.50, 14.00, 14.50, 15.00, 15.50, 16.00, 16.50, 17.00, 17.50, 18.00, 18.50, 19.00, 19.50, 20.00, 20.50, 21.00, 21.50, 22.00, 22.50, 23.00, 23.50, 24.00, 24.50, 25.00, 25.50, 26.00, 26.50, 27.00, 27.50, 28.00, 28.50, 29.00, 29.50, 30.00, 30.50, 31.00, 31.50, 32.00, 32.50, 33.00, 33.50, 34.00, 34.50, 35.00, 35.50, 36.00, 36.50, 37.00, 37.50, 38.00, 38.50, 39.00, 39.50, 40.00, 40.50, 41.00, 41.50, 42.00, 42.50, 43.00, 43.50, 44.00, 44.50, 45.00, 45.50, 46.00, 46.50, 47.00, 47.50, 48.00, 48.50, 49.00, 49.50];
Hour = 0.000277;
KalmanErrorEstimate = 10;
KalmanErrorMeasurement = 5;



for ( i = 1:1:99 ) 
    
 KalmanMeasurement = Current(i)*dt*Hour;
 Coulomb(i) = KalmanMeasurement/(dt*Hour);
 KalmanGain = KalmanErrorEstimate /(KalmanErrorEstimate + KalmanErrorMeasurement);
 KalmanEstimate = KalmanEstimate + KalmanGain*(KalmanMeasurement - KalmanEstimate);
 KalmanErrorEstimate = (1-KalmanGain)*KalmanErrorEstimate;
 Measurement(i) = KalmanEstimate/(dt*Hour);
end


figure;
hold on;
plot(Time,Coulomb);
plot(Time,Measurement);
legend('Current','Measurement');