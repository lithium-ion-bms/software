function OpenTBF(app, obj, evt)

global TestBatteryFile

TestBatteryFile = figure();
TestBatteryFile.Name = 'Open .tbf';
TestBatteryFile.NumberTitle = 'off';
set(TestBatteryFile,'Menubar','None');
TestBatteryFile.Units = ('normalized');
TestBatteryFile.Position = [0.45 0.45 0.2 0.1];

OpenFile = uicontrol;
OpenFile.Style = ('pushbutton');
OpenFile.Units = ('normalized');
OpenFile.Position = [0 0 1 1];
OpenFile.String = ('Open .TBF');
OpenFile.Callback = @OpenfileCallback;
end
