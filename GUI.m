clear all; clc; close all;
%% Global variables
global CellInfo
global CellVoltage
global MinCellVoltage
global MaxCellVoltage
global SenseTemp
global CellSoC
global CellSoH
global SoCSoH
global SystemCurrent
global FreqCount
global TimerCount
global Cell
global NumberOfTemp
global NoError
global MinorError
global CriticalError
global MC
global Freq
global Fault
global LastCellInfo
global MinV
global MaxV
global AvgV
global AvgVoltage
global MaxVoltage
global MinVoltage
global OperationMode
global ConnectUps
global SystemVoltage
global SystemV
global Filename
global StateOfHealthValue
global StateOfChargeValue

MC = serial('COM13');
MC.baudrate = 115200;
MC.timeout = 1;
MC.OutputBufferSize = 1024;
fopen(MC);

Filename = sprintf('Test_%s.TBF', datestr(now,'mm-dd-yyyy'));

cd BatteryTestFile
edit(Filename);
fileID = fopen(Filename, 'w');
fprintf(fileID,'Time Current SystemVoltage SoC SoH Temp Cell 1 Cell 2 Cell 3 Cell 4 Cell 5 Cell 6 Cell 7 Cell 8 Cell 9 Cell 10 Cell 11 Cell 12\n');
fclose(fileID);
cd ..


FreqCount = 0;
Freq = timer;

setGlobalSoC(0.5);
answer = {'6','2','3.6', '2.6', '3'};
setGlobalCellInfo(answer);
%Cell = 1:1:6;

CellInfoDouble  = str2double(CellInfo);
LastCellInfo    = CellInfoDouble;
SenseTemp       = 1:1:CellInfoDouble(5);

MaxCellVoltage=zeros((CellInfoDouble(1)*CellInfoDouble(2)),1);
MinCellVoltage=zeros((CellInfoDouble(1)*CellInfoDouble(2)),1);
for i = 1:1:(CellInfoDouble(1)*CellInfoDouble(2))
MinCellVoltage(i) = 4;
end
CellVoltage=zeros(CellInfoDouble(1),1);
MinVoltage = 4; 
MaxVoltage = 0; 
AvgVoltage = 2;
Cell            = 1:1:(CellInfoDouble(1)*CellInfoDouble(2));
NumberOfTemp    = 1:1:(CellInfoDouble(5)*CellInfoDouble(2));
CellSoC         = 1:1:CellInfoDouble(1);
CellSoH         = 1:1:CellInfoDouble(1);
TimerCount      = 0.5:0.5:FreqCount/2;
SystemCurrent   = 0;
Fault = 3;
SingleSoH = 0;
SingleSoc = 0;

%% Figure setup
FigGui = figure();
    FigGui.Name = 'Measured Data';
    FigGui.NumberTitle = 'off';
    FigGui.Units = 'normalized';
    FigGui.OuterPosition  = [0 0 1 1];
    FigGui.MenuBar = 'none';
 %% Subplot setup   
Voltage = subplot(2,2,1);
    %plot(Cell,'XDataSource','Cell','YDataSource','CellVoltage')
    %linkdata on;
    Votlage.Units = 'normalized';
    Voltage.Position = [ 0.04 0.60 0.35 0.35];
    NumberOfCell = Cell;
    hold on;
    bar(NumberOfCell,'r','XDataSource','Cell','YDataSource','MaxCellVoltage');
    bar(NumberOfCell, 'y', 'XDataSource','Cell','YDataSource','CellVoltage');
    bar(NumberOfCell, 'g','XDataSource','Cell','YDataSource','MinCellVoltage');
    linkdata on;
    legend 'MaxVoltage' 'Voltage' 'MinVoltage';
    ylabel('Voltage [V]');
    xlabel('Cell [#]');
    ylim([ 2.0 4.0]);
    
Temp = subplot(2,2,2);
    plot(NumberOfTemp,'XDataSource','NumberOfTemp','YDataSource','SenseTemp')
    linkdata on;
    Temp.Units = 'normalized';
    Temp.Position = [ 0.5 0.60 0.35 0.35];
    ylabel('Temperature [c]');
    xlabel('Temperature Sensor [#]');
    ylim([10 60]);
    
    
Current = subplot(2,2,3);
    plot(SystemCurrent,'XDataSource','TimerCount','YDataSource','SystemCurrent'); 
    linkdata on;
    Current.Units = 'normalized';
    Current.Position = [ 0.04 0.15 0.35 0.35];
    ylabel('Current [mA]');
    xlabel('Time [Sec]');

SoCSoH = subplot(2,2,4);
    SoCSoH.Units = 'normalized';
    SoCSoH.Position = [0.5 0.15 0.35 0.35];
    XValue = categorical({'SoC','SoH'});
    SoCSoH = [SingleSoc SingleSoH];
    bar(XValue,SoCSoH)
    linkdata on;
    ylabel('Procent')
    ylim([0 100]);

  %% Button Setup  
  
 btnCell = uicontrol(); 
    btnCell.Style = 'pushbutton';
    btnCell.String = 'Number of Cell';
    btnCell.Units = 'normalized';
    btnCell.Position = [ 0.025 0.030 0.070 0.065];
    btnCell.Callback = @NumberOfCellCallBack;
  
 SysSoc = uicontrol(); 
    SysSoc.Style = 'pushbutton';
    SysSoc.String = 'Set System SoC';
    SysSoc.Units = 'normalized';
    SysSoc.Position = [ 0.165 0.030 0.070 0.065];
    SysSoc.Callback = @SystemSoCCallback;
    
  
 ConnectUps = uicontrol(); 
    ConnectUps.Style = 'togglebutton';
    ConnectUps.String = 'Connected'; % Write Disconnected when disconnected & Connected when connected
    ConnectUps.BackgroundColor = 'g';
    ConnectUps.Units = 'normalized';
    ConnectUps.Tag = 'Button1';
    ConnectUps.Position = [ 0.305 0.030 0.070 0.065]; 
    ConnectUps.Callback = @ConnectCallback;

   
    
  CloseConnection = uicontrol(); 
    CloseConnection.Style = 'pushbutton';
    CloseConnection.String = 'Close';
    CloseConnection.Units = 'normalized';
    CloseConnection.Position = [ 0.445 0.030 0.070 0.065];
    CloseConnection.Callback = @CloseConnectionCallBack;
    
  ResetCommunication = uicontrol(); 
    ResetCommunication.Style = 'pushbutton';
    ResetCommunication.String = 'Reset';
    ResetCommunication.Units = 'normalized';
    ResetCommunication.Position = [ 0.585 0.030 0.070 0.065];
    ResetCommunication.Callback = @ResetCommunicationCallBack;  
    
  TestMode = uicontrol(); 
    TestMode.Style = 'pushbutton';
    TestMode.String = 'Test Mode';
    TestMode.Units = 'normalized';
    TestMode.Position = [ 0.725 0.030 0.070 0.065];
    TestMode.Callback = @OpenTBF;
    
    
    
  %% Info Panel Setup
  
  
   NoError = uicontrol(); 
    NoError.Style = 'pushbutton';
    NoError.BackgroundColor = 'G'; % Change when error detected 'g' When ok
    NoError.Units = 'normalized';
    NoError.Position = [0.90 0.875 0.070 0.075];
   NoErrorText = uicontrol();
    NoErrorText.Style = 'Text';
    NoErrorText.String = 'No Error';
    NoErrorText.Units = 'normalized';
    NoErrorText.Position = [0.90 0.95 0.070 0.015];
    
   MinorError = uicontrol(); 
    MinorError.Style = 'pushbutton';
    MinorError.BackgroundColor = 'W'; % Change when error detected 'y' when minor error
    MinorError.Units = 'normalized';
    MinorError.Position = [0.9 0.735 0.070 0.075];
   MinorErrorText = uicontrol();
    MinorErrorText.Style = 'Text';
    MinorErrorText.String = 'Minor Error';
    MinorErrorText.Units = 'normalized';
    MinorErrorText.Position = [0.90 0.81 0.070 0.015];
    
   CriticalError = uicontrol(); 
    CriticalError.Style = 'pushbutton';
    CriticalError.BackgroundColor = 'W'; % Change when error detected 'r' when critical error
    CriticalError.Units = 'normalized';
    CriticalError.Position = [0.9 0.595 0.070 0.075];
    CriticalErrorText = uicontrol();
    CriticalErrorText.Style = 'Text';
    CriticalErrorText.String = 'Critical Error';
    CriticalErrorText.Units = 'normalized';
    CriticalErrorText.Position = [0.90 0.67 0.070 0.015];
    
    OperationMode = uicontrol(); 
     OperationMode.Style = 'Pushbutton';
     OperationMode.String = 'Standalone';
     OperationMode.BackgroundColor = 'w'; % Change when error detected
     OperationMode.Units = 'normalized';
     OperationMode.Tag = 'Operationmode';
     OperationMode.Position = [0.9 0.455 0.070 0.075];
    
     
    MinVText = uicontrol(); 
     MinVText.Style = 'Text';
     MinVText.String = 'Min Voltage';
     MinVText.Units = 'normalized';
     MinVText.Position = [0.4 0.85 0.070 0.020];
     
    MinV = uicontrol(); 
     MinV.Style = 'Pushbutton';
     MinV.String = MinVoltage;
     linkdata on;
     MinV.BackgroundColor = 'w'; % Change when error detected
     MinV.Units = 'normalized';
     MinV.Tag = 'MinV';
     MinV.Position = [0.4 0.83 0.070 0.020];
    
    AvgVText = uicontrol(); 
     AvgVText.Style = 'Text';
     AvgVText.String = 'Avg Voltage';
     AvgVText.Units = 'normalized';
     AvgVText.Position = [0.4 0.89 0.070 0.020];
     
    AvgV = uicontrol(); 
     AvgV.Style = 'Pushbutton';
     AvgV.String = AvgVoltage;
     AvgV.BackgroundColor = 'w'; % Change when error detected
     AvgV.Units = 'normalized';
     AvgV.Tag = 'AvgV';
     AvgV.Position = [0.4 0.87 0.070 0.020];
     
    MaxVText = uicontrol(); 
     MaxVText.Style = 'Text';
     MaxVText.String = 'Max Voltage';
     MaxVText.Units = 'normalized';
     MaxVText.Position = [0.4 0.93 0.070 0.020];
     
    MaxV = uicontrol(); 
     MaxV.Style = 'Pushbutton';
     MaxV.String = MaxVoltage;
     MaxV.BackgroundColor = 'w'; % Change when error detected
     MaxV.Units = 'normalized';
     MaxV.Tag = 'MaxV';
     MaxV.Position = [0.4 0.91 0.070 0.020];
     
       AvgV = uicontrol(); 
     AvgV.Style = 'Pushbutton';
     AvgV.String = AvgVoltage;
     AvgV.BackgroundColor = 'w'; % Change when error detected
     AvgV.Units = 'normalized';
     AvgV.Tag = 'AvgV';
     AvgV.Position = [0.4 0.87 0.070 0.020];
     
    SystemVText = uicontrol(); 
     SystemVText.Style = 'Text';
     SystemVText.String = 'System Voltage';
     SystemVText.Units = 'normalized';
     SystemVText.Position = [0.4 0.81 0.070 0.020];
     
    SystemV = uicontrol(); 
     SystemV.Style = 'Pushbutton';
     SystemV.String = SystemVoltage;
     SystemV.BackgroundColor = 'w'; % Change when error detected
     SystemV.Units = 'normalized';
     SystemV.Position = [0.4 0.79 0.070 0.020];
     
   StateOfChargeValue = uicontrol(); 
     StateOfChargeValue.Style = 'Pushbutton';
     StateOfChargeValue.String = SoCSoH(1);
     StateOfChargeValue.BackgroundColor = 'w'; 
     StateOfChargeValue.Units = 'normalized';
     StateOfChargeValue.Position = [0.538 0.50 0.10 0.035];  
     
   StateOfHealthValue = uicontrol(); 
     StateOfHealthValue.Style = 'Pushbutton';
     StateOfHealthValue.String = SoCSoH(2);
     StateOfHealthValue.BackgroundColor = 'w'; 
     StateOfHealthValue.Units = 'normalized';
     StateOfHealthValue.Position = [0.714 0.50 0.10 0.035];
     
   %% Setup and start timer
   
  Freq.StartDelay = 1;
  Freq.TimerFcn = @MatlabMonitoring;
  Freq.Period = 0.010;
  %Freq.TasksToExecute = 280; % Set number of "replay"
  Freq.ExecutionMode = ('fixedSpacing');
  
  start(Freq)
  
 
  