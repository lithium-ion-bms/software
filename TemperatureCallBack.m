function TemperatureCallBack(app, obj, evt)


global Temperature
global Time


TemperatureGraph = figure();
TemperatureGraph.Name = 'Current';
set(TemperatureGraph,'Menubar','None');
TemperatureGraph.Units = ('normalized');
TemperatureGraph.Position = [0 0 1 1];

plot(Time,Temperature);
xlabel('Time [s]');
ylabel('Temperature [C]');

end
