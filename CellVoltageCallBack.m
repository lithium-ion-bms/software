function CellVoltageCallBack(app, obj, evt)
global Cell1
global Cell2
global Cell3
global Cell4
global Cell5
global Cell6
global Cell7
global Cell8
global Cell9
global Cell10
global Cell11
global Cell12
global Time
global CellVoltage
global CellOne
global CellTwo
global CellThree
global CellFour
global CellFive
global CellSix
global CellSeven
global CellEight
global CellNine
global CellTen
global CellElleven
global CellTwelfe
global All

CellVoltageGraph = figure();
CellVoltageGraph.Name = 'Current';
set(CellVoltageGraph,'Menubar','None');
CellVoltageGraph.Units = ('normalized');
CellVoltageGraph.Position = [0 0 1 1];


hold on;
CellOne = plot(Time,CellVoltage(:,1));
CellTwo = plot(Time,CellVoltage(:,2));
CellThree = plot(Time,CellVoltage(:,3));
CellFour = plot(Time,CellVoltage(:,4));
CellFive = plot(Time,CellVoltage(:,5));
CellSix = plot(Time,CellVoltage(:,6));
CellSeven = plot(Time,CellVoltage(:,7));
CellEight = plot(Time,CellVoltage(:,8));
CellNine = plot(Time,CellVoltage(:,9));
CellTen = plot(Time,CellVoltage(:,10));
CellElleven = plot(Time,CellVoltage(:,11));
CellTwelfe = plot(Time,CellVoltage(:,12));
xlabel('Time [s]');
ylabel('Voltage [V]');
legend('Cell 1','Cell 2','Cell 3','Cell 4','Cell 5','Cell 6','Cell 7','Cell 8','Cell 9','Cell 10','Cell 11','Cell 12')


Cell1 = uicontrol;
Cell1.Style = ('Checkbox');
Cell1.Value = 1;
Cell1.Units = ('normalized');
Cell1.Position = [0.01 0.9 0.05 0.02];
Cell1.String = ('Cell 1');
Cell1.Callback = @CellOneCallBack;
 
 

Cell2 = uicontrol;
Cell2.Style = ('Checkbox');
Cell2.Value = 1;
Cell2.Units = ('normalized');
Cell2.Position = [0.01 0.85 0.05 0.02];
Cell2.String = ('Cell 2');
Cell2.Callback = @CellTwoCallBack;


Cell3 = uicontrol;
Cell3.Style = ('Checkbox');
Cell3.Value = 1;
Cell3.Units = ('normalized');
Cell3.Position = [0.01 0.8 0.05 0.02];
Cell3.String = ('Cell 3');
Cell3.Callback = @CellThreeCallBack;


Cell4 = uicontrol;
Cell4.Style = ('Checkbox');
Cell4.Value = 1;
Cell4.Units = ('normalized');
Cell4.Position = [0.01 0.75 0.1 0.02];
Cell4.String = ('Cell 4');
Cell4.Callback = @CellFourCallBack;
% 

Cell5 = uicontrol;
Cell5.Style = ('Checkbox');
Cell5.Value = 1;
Cell5.Units = ('normalized');
Cell5.Position = [0.01 0.70 0.05 0.02];
Cell5.String = ('Cell 5');
Cell5.Callback = @CellFiveCallBack;
% 

Cell6 = uicontrol;
Cell6.Style = ('Checkbox');
Cell6.Value = 1;
Cell6.Units = ('normalized');
Cell6.Position = [0.01 0.65 0.05 0.02];
Cell6.String = ('Cell 6');
Cell6.Callback = @CellSixCallBack;
% 

Cell7 = uicontrol;
Cell7.Style = ('Checkbox');
Cell7.Value = 1;
Cell7.Units = ('normalized');
Cell7.Position = [0.01 0.60 0.05 0.02];
Cell7.String = ('Cell 7');
Cell7.Callback = @CellSevenCallBack;
% 

Cell8 = uicontrol;
Cell8.Style = ('Checkbox');
Cell8.Value = 1;
Cell8.Units = ('normalized');
Cell8.Position = [0.01 0.55 0.05 0.02];
Cell8.String = ('Cell 8');
Cell8.Callback = @CellEightCallBack;
% 

Cell9 = uicontrol;
Cell9.Style = ('Checkbox');
Cell9.Value = 1;
Cell9.Units = ('normalized');
Cell9.Position = [0.01 0.50 0.05 0.02];
Cell9.String = ('Cell 9');
Cell9.Callback = @CellNineCallBack;
%

Cell10 = uicontrol;
Cell10.Style = ('Checkbox');
Cell10.Value = 1;
Cell10.Units = ('normalized');
Cell10.Position = [0.01 0.45 0.05 0.02];
Cell10.String = ('Cell 10');
Cell10.Callback = @CellTenCallBack;
% 

Cell11 = uicontrol;
Cell11.Style = ('Checkbox');
Cell11.Value = 1;
Cell11.Units = ('normalized');
Cell11.Position = [0.01 0.40 0.05 0.02];
Cell11.String = ('Cell 11');
Cell11.Callback = @CellEllevenCallBack;
% 

Cell12 = uicontrol;
Cell12.Style = ('Checkbox');
Cell12.Value = 1;
Cell12.Units = ('normalized');
Cell12.Position = [0.01 0.35 0.05 0.02];
Cell12.String = ('Cell 12');
Cell12.Callback = @CellTwelfeCallBack;

All = uicontrol;
All.Style = ('Checkbox');
All.Value = 1;
All.Units = ('normalized');
All.Position = [0.01 0.30 0.05 0.02];
All.String = ('Select/Deselect all');
All.Callback = @AllCallBack;

end

function CellOneCallBack(app, obj, evt)


global Cell1
global CellOne


if ( Cell1.Value == 0 ) 
    CellOne.Visible = 'off';
else
    CellOne.Visible = 'on';
end 
end

function CellTwoCallBack(app, obj, evt)

global Cell2
global CellTwo


if ( Cell2.Value == 0 ) 
    CellTwo.Visible = 'off';
else
    CellTwo.Visible = 'on';
end 
end

function CellThreeCallBack(app, obj, evt)

global Cell3
global CellThree


if ( Cell3.Value == 0 ) 
    CellThree.Visible = 'off';
else
    CellThree.Visible = 'on';
end 
end

function CellFourCallBack(app, obj, evt)

global Cell4
global CellFour


if ( Cell4.Value == 0 ) 
    CellFour.Visible = 'off';
else
    CellFour.Visible = 'on';
end 
end

function CellFiveCallBack(app, obj, evt)

global Cell5
global CellFive


if ( Cell5.Value == 0 ) 
    CellFive.Visible = 'off';
else
    CellFive.Visible = 'on';
end 
end

function CellSixCallBack(app, obj, evt)

global Cell6
global CellSix


if ( Cell6.Value == 0 ) 
    CellSix.Visible = 'off';
else
    CellSix.Visible = 'on';
end 
end

function CellSevenCallBack(app, obj, evt)

global Cell7
global CellSeven


if ( Cell7.Value == 0 ) 
    CellSeven.Visible = 'off';
else
    CellSeven.Visible = 'on';
end 
end

function CellEightCallBack(app, obj, evt)

global Cell8
global CellEight


if ( Cell8.Value == 0 ) 
    CellEight.Visible = 'off';
else
    CellEight.Visible = 'on';
end 
end

function CellNineCallBack(app, obj, evt)

global Cell9
global CellNine


if ( Cell9.Value == 0 ) 
    CellNine.Visible = 'off';
else
    CellNine.Visible = 'on';
end 
end

function CellTenCallBack(app, obj, evt)

global Cell10
global CellTen


if ( Cell10.Value == 0 ) 
    CellTen.Visible = 'off';
else
    CellTen.Visible = 'on';
end 
end

function CellEllevenCallBack(app, obj, evt)

global Cell11
global CellElleven


if ( Cell11.Value == 0 ) 
    CellElleven.Visible = 'off';
else
    CellElleven.Visible = 'on';
end 
end

function CellTwelfeCallBack(app, obj, evt)

global Cell12
global CellTwelfe


if ( Cell12.Value == 0 ) 
    CellTwelfe.Visible = 'off';
else
    CellTwelfe.Visible = 'on';
end 
end

function AllCallBack(app, obj, evt)

global All
global Cell1
global Cell2
global Cell3
global Cell4
global Cell5
global Cell6
global Cell7
global Cell8
global Cell9
global Cell10
global Cell11
global Cell12

if ( All.Value == 1 ) 
 Cell1.Value = 1;
 Cell2.Value = 1;
 Cell3.Value = 1;
 Cell4.Value = 1;
 Cell5.Value = 1;
 Cell6.Value = 1;
 Cell7.Value = 1;
 Cell8.Value = 1;
 Cell9.Value = 1;
 Cell10.Value = 1;
 Cell11.Value = 1;
 Cell12.Value = 1;
else
 Cell1.Value = 0;
 Cell2.Value = 0;
 Cell3.Value = 0;
 Cell4.Value = 0;
 Cell5.Value = 0;
 Cell6.Value = 0;
 Cell7.Value = 0;
 Cell8.Value = 0;
 Cell9.Value = 0;
 Cell10.Value = 0;
 Cell11.Value = 0;
 Cell12.Value = 0;
end

CellOneCallBack;
CellTwoCallBack;
CellThreeCallBack;
CellFourCallBack;
CellFiveCallBack;
CellSixCallBack;
CellSevenCallBack;
CellEightCallBack;
CellNineCallBack;
CellTenCallBack;
CellEllevenCallBack;
CellTwelfeCallBack;

end