FigGui = figure();
    FigGui.Name = 'Select Mode';
    FigGui.NumberTitle = 'off';
    FigGui.Units = 'normalized';
    FigGui.OuterPosition  = [0.25 0.25 0.5 0.5];
    FigGui.MenuBar = 'none';
    
    MCUConnect = uicontrol(); 
     MCUConnect.Style = 'Pushbutton';
     MCUConnect.String = 'Connect To MCU';
     MCUConnect.BackgroundColor = 'w'; % Change when error detected
     MCUConnect.Units = 'normalized';
     MCUConnect.Position = [0 0 0.5 1];
     MCUConnect.Callback = @McuConnectCallback;
     
    TestMode = uicontrol(); 
     TestMode.Style = 'Pushbutton';
     TestMode.String = 'Open Test Mode';
     TestMode.BackgroundColor = 'w'; % Change when error detected
     TestMode.Units = 'normalized';
     TestMode.Position = [0.5 0 0.5 1];
     TestMode.Callback = @OpenTBF;
     
     function McuConnectCallback(app, obj, evt)
     %close all
     run('GUI.m');
     end