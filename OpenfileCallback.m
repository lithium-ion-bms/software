function OpenfileCallback(app, obj, evt)

global filename
global Time
global Current
global SystemVoltage
global Temperature
global StateOfCharge
global StateOfHealth
global CellVoltage



NumHeaders = 1;     %for example
NumDataLines = 1234; %for example
ColNum = 1;          %for example

[filename,Pathname] = uigetfile('*.TBF','Select .tbf file');

str = [Pathname,filename];
filename = join(str,"\");

SelectGraph = figure();
SelectGraph.Name = 'SelectGraphName';
SelectGraph.NumberTitle = 'off';
set(SelectGraph,'Menubar','None');
SelectGraph.Units = ('normalized');
SelectGraph.Position = [0.15 0.45 0.6 0.1];

CVT = uicontrol;
CVT.Style = ('pushbutton');
CVT.Units = ('normalized');
CVT.Position = [0 0 0.16 1];
CVT.String = ('Current VS Time');
CVT.Callback = @CurrentVsTimeCallback;

SVVT = uicontrol;
SVVT.Style = ('pushbutton');
SVVT.Units = ('normalized');
SVVT.Position = [0.16 0 0.16 1];
SVVT.String = ('Sys Voltage VS Time');
 SVVT.Callback = @SystemVoltageCallBack;

SOCVT = uicontrol;
SOCVT.Style = ('pushbutton');
SOCVT.Units = ('normalized');
SOCVT.Position = [0.32 0 0.16 1];
SOCVT.String = ('SoC VS Time');
SOCVT.Callback = @StateOfChargeCallBack;

SOHVT = uicontrol;
SOHVT.Style = ('pushbutton');
SOHVT.Units = ('normalized');
SOHVT.Position = [0.48 0 0.16 1];
SOHVT.String = ('SoH VS Time');
SOHVT.Callback = @StateOfHealthCallBack;

TVT = uicontrol;
TVT.Style = ('pushbutton');
TVT.Units = ('normalized');
TVT.Position = [0.64 0 0.16 1];
TVT.String = ('Temp VS Time');
TVT.Callback = @TemperatureCallBack;

CVVT = uicontrol;
CVVT.Style = ('pushbutton');
CVVT.Units = ('normalized');
CVVT.Position = [0.80 0 0.16 1];
CVVT.String = ('Cell Voltage VS Time');
CVVT.Callback = @CellVoltageCallBack;



for ColNum = 1:1:18
fmt = [ repmat('%*s',1,ColNum-1), '%f%[^\n]'];
fid = fopen(filename, 'rt');
data = textscan(fid, fmt, NumDataLines, 'HeaderLines', NumHeaders);
fclose(fid);
TempData(:,ColNum) = data{1};
end

Time = TempData(:,1);
Current = TempData(:,2);
SystemVoltage = TempData(:,3);
StateOfCharge = TempData(:,4);
StateOfHealth = TempData(:,5);
Temperature = TempData(:,6);

for Cell = 1:1:12
CellVoltage(:,Cell) = TempData(:,6+Cell);
end


end