function StateOfChargeCallBack(app, obj, evt)

global StateOfCharge
global Time


StateOfChargeGraph = figure();
StateOfChargeGraph.Name = 'Current';
set(StateOfChargeGraph,'Menubar','None');
StateOfChargeGraph.Units = ('normalized');
StateOfChargeGraph.Position = [0 0 1 1];

plot(Time,StateOfCharge);
xlabel('Time [s]');
ylabel('SoC [%]');

end
