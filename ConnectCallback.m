function ConnectCallback(hObject,callbackdata)
   global MC
    
    disp(hObject.String);
    handles = guihandles(hObject);
    IsPushed = handles.Button1.Value;
    setGlobalConnect(IsPushed);
    Header      = hex2dec('01');  
    
    if IsPushed
        handles.Button1.String = 'Disconnected';
        handles.Button1.BackgroundColor = 'r';
        handles.Operationmode.String = 'Hibernation';
        ByteOne     = hex2dec('00');    
    else 
        handles.Button1.String = 'Connected';
        handles.Button1.BackgroundColor = 'g';
        handles.Operationmode.String = 'Standalone';
        ByteOne     = hex2dec('01');
    end
    
    fwrite(MC, Header)
    fwrite(MC, ByteOne)

end
