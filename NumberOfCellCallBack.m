function NumberOfCellCallBack(app, obj, evt)
    global CellInfo
    global Cell
    global MC
    
    

    answer = CellInfo;
    prompt = {'Number of cells:','Number of BMS board:', 'Voltage Max lvl:', 'Voltage Min lvl:','Number of Temperature Sense'};
    dlg_title = 'Input';
    num_lines = 1;
    defaultans  = answer;
    answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
    setGlobalCellInfo(answer);
    
    CellInfoDouble = str2double(CellInfo);
    %Cell        = 1:1:(CellInfoDouble(1)*CellInfoDouble(2));
    
%     for i = 1:CellInfoDouble(1)
%      MaxCellVoltage(i) = CellInfoDouble(3);
%      MinCellVoltage(i) = CellInfoDouble(4);
%     end
    
    Header      = hex2dec('25');
    ByteOne     = CellInfoDouble(1);
    ByteTwo     = CellInfoDouble(2);
    ByteThree   = CellInfoDouble(3)*10;
    ByteFour    = CellInfoDouble(4)*10;
    ByteFive    = CellInfoDouble(5);

    fwrite(MC,Header);
    fwrite(MC,ByteOne);
    fwrite(MC,ByteTwo);
    fwrite(MC,ByteThree);
    fwrite(MC,ByteFour);
    fwrite(MC,ByteFive);
    
end 
