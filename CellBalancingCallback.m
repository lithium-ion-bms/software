function CellBalancingCallback(app, obj, evt)

   global CellSoH
   
CellBal = figure(2);
    CellBal.Name = 'Cell SoH';
    CellBal.NumberTitle = 'off';
    CellBal.Units = 'normalized';
    CellBal.OuterPosition  = [0 0 1 1];
    CellBal.MenuBar = 'none';
    
    
    
CellSoc = plot(CellSoH,'XDataSource','TimerCount','YDataSource','CellSoH');
    ylabel('SoH [%]');
    xlabel('Cell [#]');
    ylim([0 100]);
    xlim([0 12]);
    title('State of Health per cell');

 ExitBut = uicontrol();
    ExitBut.Style = 'pushbutton';
    ExitBut.String = 'Exit';
    ExitBut.Units = 'normalized';
    ExitBut.Position = [ 0.93 0 0.07 1];
    ExitBut.Callback = @ExitCallBack;    

end

function ExitCallBack( app, obj, evt)
    close 'Cell SoH';
end 
