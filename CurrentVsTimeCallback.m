function CurrentVsTimeCallback(app, obj, evt)
global Time
global Current


CurrentGraph = figure();
CurrentGraph.Name = 'Current';
set(CurrentGraph,'Menubar','None');
CurrentGraph.Units = ('normalized');
CurrentGraph.Position = [0 0 1 1];

plot(Time,Current);
xlabel('Time [s]');
ylabel('Current [A]');




end