function StateOfHealthCallBack(app, obj, evt)


global StateOfHealth
global Time


StateOfHealthGraph = figure();
StateOfHealthGraph.Name = 'Current';
set(StateOfHealthGraph,'Menubar','None');
StateOfHealthGraph.Units = ('normalized');
StateOfHealthGraph.Position = [0 0 1 1];

plot(Time,StateOfHealth);
xlabel('Time [s]');
ylabel('SoH [%]');

end
