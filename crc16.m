% Construct a CRC generator with a polynomial defined
% by x^4+x^3+x^2+x+1:
h = crc.generator([1 1 1 1 1]) 
% Construct a CRC generator with a polynomial defined
% by x^4+x^3+x^2+x+1, all-ones initial states, reflected
% input, and all-zeros final XOR value:
h = crc.generator('Polynomial', '0xF', 'InitialState', ...
'0xF', 'ReflectInput', true, 'FinalXOR', '0x0')
% Create a CRC-16 CRC generator, then use it to generate
% a checksum for the
% binary vector represented by the ASCII sequence '123456789'.
gen = crc.generator('Polynomial', '0x8005', ...
'ReflectInput', true, 'ReflectRemainder', true);
% The message below is an ASCII representation of ...
% the digits 1-9
msg = reshape(de2bi(49:57, 8, 'left-msb')', 72, 1);
encoded = generate(gen, msg);
% Construct a CRC generator with a polynomial defined
% by x^3+x+1, with zero initial states,
% and with an all-ones final XOR value:
h = crc.generator('Polynomial', [1 0 1 1], ...
                   'InitialState', [0 0 0], ...
                   'FinalXOR', [1 1 1])