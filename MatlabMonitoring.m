function MatlabMonitoring(app, obj, evt)

    global CellInfo
    global CellVoltage
    global MinCellVoltage
    global MaxCellVoltage
    global SenseTemp
   global CellSoC
   global CellSoH
   global TimerCount
   global FreqCount
   global SystemCurrent
global SoCSoH
global NoError
global MinorError
global CriticalError
global MC
global Fault
global LastCellInfo
global Cell
global NumberOfTemp
global AvgVoltage
global MaxVoltage
global MinVoltage
global MinV
global MaxV
global AvgV
global OperationMode
global ConnectUps
global SystemVoltage
global SystemV
global Filename
global StateOfHealthValue
global StateOfChargeValue
global SoC

 Data = {0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0};


   CellInfoDouble = str2double(CellInfo);
   CellVoltage  = 1:1:CellInfoDouble(1);
   SenseTemp    = 1:1:CellInfoDouble(5);
    Header = hex2dec('90');

    fwrite(MC,Header);
    Data = fread(MC);

   if ( Data(2) == 0 )
   else
      if ( LastCellInfo == CellInfoDouble ) 
         Cell = 1:1:(CellInfoDouble(1)*CellInfoDouble(2));
         NumberOfTemp = 1:1:(CellInfoDouble(5)*CellInfoDouble(2));
      for i = 1:1:Data(2)
          
          if i < ((CellInfoDouble(1)*CellInfoDouble(2)) + 1)
          CellVoltage(i) = (Data(i*2+2)*2^8+Data(i*2+1))/1000;
          
          if CellVoltage(i) > MaxCellVoltage(i)
              MaxCellVoltage(i) = CellVoltage(i);
              MinCellVoltage(i) = MinCellVoltage(i) + 1 -1;
          elseif CellVoltage(i) < MinCellVoltage(i)
              MinCellVoltage(i) = CellVoltage(i);
          end
          count = i*2+2;
          
          elseif i < ((CellInfoDouble(1)*CellInfoDouble(2))+(CellInfoDouble(5)*CellInfoDouble(2))+1)
          SenseTemp(i-(CellInfoDouble(1)*CellInfoDouble(2))) = (Data(count+1))-10;
          count = count+1;
          
          elseif i <  ( (CellInfoDouble(1)*CellInfoDouble(2))+(CellInfoDouble(5)*CellInfoDouble(2))+2)
          CellSoC = Data(count+1);
          SoCSoH(1) = CellSoC;
          count = count+1;  
          
          elseif i <  (CellInfoDouble(1)*CellInfoDouble(2))+(CellInfoDouble(5)*CellInfoDouble(2))+3
          CellSoH = Data(count+1);
          SoCSoH(2) = CellSoH;
          count = count+1;
          
          elseif i < (CellInfoDouble(1)*CellInfoDouble(2))+(CellInfoDouble(5)*CellInfoDouble(2))+4
          FreqCount = FreqCount + 1;
          TimerCount = 0.5:0.5:FreqCount/2;
          SystemCurrent(FreqCount)   = Data(count+1)+Data(count+2)*2^8;
          count = count+1;
          
          elseif i < (CellInfoDouble(1)*CellInfoDouble(2))+(CellInfoDouble(5)*CellInfoDouble(2))+5
          Fault = Data(count+2);
          count = count+1;
        
          elseif i < (CellInfoDouble(1)*CellInfoDouble(2))+(CellInfoDouble(5)*CellInfoDouble(2))+6
          OpMode = Data(count+2);
          count = count+1;
          
          elseif i < (CellInfoDouble(1)*CellInfoDouble(2))+(CellInfoDouble(5)*CellInfoDouble(2))+7
              SystemVoltage  = Data(count+3)*2^8+Data(count+2);
            SystemV.String = SystemVoltage;
          end        
      end
      
      if ( OpMode == 1 )
          OperationMode.String = 'Hibernation';
          ConnectUps.String = 'Disconnected';
          ConnectUps.BackgroundColor = 'r';
      elseif ( OpMode == 2 ) 
        OperationMode.String = 'UPS Connected';
      elseif ( OpMode == 3 ) 
        OperationMode.String = 'Standalone';
        ConnectUps.String = 'Connected';
        ConnectUps.BackgroundColor = 'g';
      end
      
      end
      if ( Fault == 1 ) 
          NoError.BackgroundColor       = 'G';
          MinorError.BackgroundColor    = 'W';
	      CriticalError.BackgroundColor = 'W';
      elseif ( Fault == 2) 
          NoError.BackgroundColor       = 'W';
          MinorError.BackgroundColor    = 'Y';
	      CriticalError.BackgroundColor = 'W';
      elseif ( Fault == 3)
          NoError.BackgroundColor       = 'W';
          MinorError.BackgroundColor    = 'W';
	      CriticalError.BackgroundColor = 'R';
      end
      
      LastCellInfo = CellInfoDouble;
      AvgVoltage = 0;
      MaxVoltage = 0;
      MinVoltage = 4;
      for i = 1:1:(CellInfoDouble(1)*CellInfoDouble(2))
          if ( MaxCellVoltage(i) > MaxVoltage ) 
              MaxVoltage = MaxCellVoltage(i);
          end 
          if (MinCellVoltage(i) < MinVoltage )
                MinVoltage = MinCellVoltage(i);
          end
          AvgVoltage = AvgVoltage + CellVoltage(i);
      end
        AvgVoltage = AvgVoltage/(CellInfoDouble(1)*CellInfoDouble(2));
        MaxV.String = MaxVoltage;
        MinV.String = MinVoltage;
        AvgV.String = AvgVoltage;   
   
        cd BatteryTestFile
        fileID = fopen(Filename, 'a');
        fprintf(fileID,'%.2f %d %d %d %d %d %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n',TimerCount(FreqCount),SystemCurrent(FreqCount),SystemVoltage,SoCSoH(1),SoCSoH(2),SenseTemp(1),CellVoltage(1),CellVoltage(2),CellVoltage(3),CellVoltage(4),CellVoltage(5),CellVoltage(6), CellVoltage(7),CellVoltage(8),CellVoltage(9),CellVoltage(10),CellVoltage(11),CellVoltage(12));
        fclose(fileID);
        cd ..
   end   
   
   if ( (SoC*100 == SoCSoH(1)) )
     if ( (OpMode == 1)||(OpMode == 3) )
       Header      = hex2dec('31');
       ByteOne     = SoC*100;
 
      fwrite(MC,Header)
      fwrite(MC,ByteOne)
     end
   end
   
     StateOfChargeValue.String = SoCSoH(1);
     StateOfHealthValue.String = SoCSoH(2);
end 