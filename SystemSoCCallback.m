
function SystemSoCCallback(app, obj, evt)
global SoC
    
Value = SoC;
FigSoc = figure(2);
    FigSoc.Name = 'Set system State of Charge';
    FigSoc.NumberTitle = 'off';
    FigSoc.Units = 'normalized';
    FigSoc.OuterPosition  = [0.3 0.5 0.4 0.2];
    FigSoc.MenuBar = 'none';

SystemSoc = uicontrol(); 
    SystemSoc.Style = 'slider';
    SystemSoc.Units = 'normalized';
    SystemSoc.Position = [ 0.0 0.0 1.0 0.7];
    SystemSoc.Tag = 'SystemSoc';
    SystemSoc.SliderStep = [0.01 0.1];
    SystemSoc.Value = Value;
    SystemSoc.Callback = @SystemSocCallBack;    
    
    
    Value = get(SystemSoc,'value');
    Procent = Value * 100;
    formatSpec = '%d%%';
    str = sprintf(formatSpec,round(Procent));
   % setGlobalSoC(str/100)
    
 SystemSocText = uicontrol();
    SystemSocText.Style = 'Text';
    SystemSocText.String = str;
    SystemSocText.Units = 'normalized';
    SystemSocText.Tag = 'SocText';
    SystemSocText.FontSize = 24;
    SystemSocText.Position = [ 0.0 0.7 0.7 0.3];
    
 ExitBut = uicontrol();
    ExitBut.Style = 'pushbutton';
    ExitBut.String = 'Set';
    ExitBut.Units = 'normalized';
    ExitBut.Position = [ 0.7 0.7 0.3 0.3];
    ExitBut.Callback = @ExitCallBack;   
end

function ExitCallBack( app, obj, evt)

    global SoC
    global MC
    
     Header      = hex2dec('31');
     ByteOne     = SoC*100;
 
      fwrite(MC,Header)
      fwrite(MC,ByteOne)
      fread(MC)
      
    close 'Set system State of Charge';
    
end 

function SystemSocCallBack(hObject, callbackdata)
    disp(hObject.String)
    handles = guihandles(hObject);
    Value = handles.SystemSoc.Value;
    setGlobalSoC(Value)
    Procent = Value * 100;
    formatSpec = '%d%%';
    str = sprintf(formatSpec,round(Procent));
    handles.SocText.String = str;
end