Filename = sprintf('Test_%s.TBF', datestr(now,'mm-dd-yyyy'));
% xlswrite(Filename, a)
cd BatteryTestFile
edit(Filename);
Current = 10;
SVoltage = 22;

 fileID = fopen(Filename, 'w');
 fprintf(fileID,'Time Current SystemVoltage SoC SoH Temp Cell1 Cell2 Cell3 Cell4 Cell5 Cell6 Cell7 Cell8 Cell9 Cell10 Cell11 Cell12\n');
 fclose(fileID);


for i=0:0.5:100
    fileID = fopen(Filename, 'a');
    fprintf(fileID,'%.2f %d %d %d %d %d %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n',i,Current,SVoltage,80,80,21,2.7,2.8,2.9,3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8);
    fclose(fileID);
end
cd ..