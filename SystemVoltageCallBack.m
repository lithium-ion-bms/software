function SystemVoltageCallBack(app, obj, evt)

global SystemVoltage
global Time


SystemVoltageGraph = figure();
SystemVoltageGraph.Name = 'Current';
set(SystemVoltageGraph,'Menubar','None');
SystemVoltageGraph.Units = ('normalized');
SystemVoltageGraph.Position = [0 0 1 1];

plot(Time,SystemVoltage);
xlabel('Time [s]');
ylabel('Voltage [V]');


end
